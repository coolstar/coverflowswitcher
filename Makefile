ARCHS = armv7 arm64
include theos/makefiles/common.mk

TWEAK_NAME = CoverFlowSwitcher
CoverFlowSwitcher_FILES = Tweak.xm
CoverFlowSwitcher_FRAMEWORKS = UIKit QuartzCore CoreGraphics

include $(THEOS_MAKE_PATH)/tweak.mk

after-install::
	install.exec "killall -9 SpringBoard"
SUBPROJECTS += 3dswitcher
include $(THEOS_MAKE_PATH)/aggregate.mk
