/* How to Hook with Logos
Hooks are written with syntax similar to that of an Objective-C @implementation.
You don't need to #include <substrate.h>, it will be done automatically, as will
the generation of a class list and an automatic constructor.
*/

#import <QuartzCore/QuartzCore.h>
#include <cstdlib>

#define SettingsPath @"/var/mobile/Library/Preferences/org.coolstar.coverflowswitcher.plist"

@interface SBAppSwitcherPageViewController : NSObject
- (CGFloat)_halfWidth;
- (void)scrollViewDidScroll:(id)anyScrollView;
- (NSUInteger)currentPage;
- (UIView *)pageViewForDisplayLayout:displayLayout;
@end

@interface SBAppSwitcherIconController : NSObject
- (CGFloat) _distanceBetweenCenters;
- (void)scrollViewDidScroll:(id)anyScrollView;
@end

@interface SBAppSwitcherController : NSObject
- (SBAppSwitcherPageViewController *)pageController;
- (SBAppSwitcherIconController *)iconController;
@end

%hook SBAppSwitcherPageViewController

- (void)scrollViewDidScroll:(id)anyScrollView {
	%orig;

	NSDictionary *preferences = [NSDictionary dictionaryWithContentsOfFile:SettingsPath];
	BOOL enabled = YES;
	if ([preferences objectForKey:@"enabled"])
		enabled = [[preferences objectForKey:@"enabled"] boolValue];
	if (!enabled)
		return;

	BOOL closerItems = NO;
	if ([preferences objectForKey:@"closerItems"])
		closerItems = [[preferences objectForKey:@"closerItems"] boolValue];

	UIScrollView *scrollView = [self valueForKey:@"_scrollView"];

	NSUInteger currentIndex = [self currentPage];

	CGFloat center = [scrollView contentOffset].x + (scrollView.frame.size.width/2.0f);
	NSUInteger y = 0;
	NSArray *displayLayouts = [self valueForKey:@"_displayLayouts"];
	for (id displayLayout in displayLayouts){
		UIView *x = [self pageViewForDisplayLayout:displayLayout];
		if (y == currentIndex)
			x.layer.zPosition = 2;
		else if (y == currentIndex+1)
			x.layer.zPosition = 1;
		else if (y == currentIndex-1)
			x.layer.zPosition = 1;
		else
			x.layer.zPosition = 0;
		y += 1;
	}

	for (UIView *x in scrollView.subviews){
		CGRect frame = x.frame;
		CGFloat centerOfView = frame.origin.x + (frame.size.width/2.0f);

		BOOL useFull3D = YES;
		BOOL invert3D = NO;
		BOOL useTrueCoverFlow = NO;
		BOOL useHalfAngle = NO;

		NSInteger mode = [[preferences objectForKey:@"mode"] integerValue];
		if (mode == 0){
			useFull3D = YES;
			invert3D = NO;
		} else if (mode == 1){
			useFull3D = NO;
			invert3D = NO;
		} else if (mode == 2){
			useFull3D = NO;
			invert3D = NO;
			useTrueCoverFlow = YES;
		} else if (mode == 3){
			useFull3D = NO;
			invert3D = YES;
			useTrueCoverFlow = YES;
		} else if (mode == 4){
			useFull3D = NO;
			invert3D = NO;
			useTrueCoverFlow = YES;
			useHalfAngle = YES;
		} else if (mode == 5){
			useFull3D = NO;
			invert3D = YES;
			useTrueCoverFlow = YES;
			useHalfAngle = YES;
		}

		CGFloat difference = centerOfView-center;

		CGFloat threshold = scrollView.superview.frame.size.width/1.9f;
		if (difference > threshold)
			difference = threshold;
		if (difference < -threshold)
			difference = -threshold;

		CGFloat maxAngle = M_PI/6.0f;
		if (useTrueCoverFlow){
			if (useHalfAngle)
				maxAngle = M_PI/8.f;
			else
				maxAngle = M_PI/4.f;
		}
		CGFloat angle = (maxAngle) * (difference/threshold);
		if (angle < 0)
			angle = (M_PI*2.0f)-angle;

		CGFloat yShift = 0.0;
		yShift = difference/50.0f;
		if (difference > 50)
			yShift = 1.0;
		if (difference < -50)
			yShift = -1.0;

		CGFloat scale = 1.0f - fabsf((float)difference)/600.f;

		CATransform3D transform = CATransform3DIdentity;
		if (useFull3D){
			transform.m34 = -1/500.0;
			transform = CATransform3DRotate(transform, angle, 1.0f, yShift, 0.0f);
			transform = CATransform3DScale(transform, scale, scale, scale);
			transform = CATransform3DTranslate(transform, 0, -(1.0f-scale)*10.0f, 0);
		} else {
			if (useTrueCoverFlow){
				if (invert3D){
					if (difference < 0)
						transform.m34 = 1/500.0;
					else
						transform.m34 = -1/500.0;
				} else {
					if (difference < 0)
						transform.m34 = -1/500.0;
					else
						transform.m34 = 1/500.0;
				}
				transform = CATransform3DRotate(transform, angle, 0.0f, 1.0f, 0.0f);
			}
			else
				transform = CATransform3DRotate(transform, angle, 1.0f, 0.0f, 0.0f);
		}
		if (closerItems){
			float zTransform = fabsf((float)difference*25.0f/(float)threshold*-1.0f);
			transform = CATransform3DTranslate(transform, 0, 0, zTransform);
		}
		if (![[[x valueForKey:@"_item"] valueForKey:@"_view"] isKindOfClass:%c(SBAppSwitcherHomePageCellView)]){
			x.layer.shouldRasterize = YES;
			x.layer.rasterizationScale = [[UIScreen mainScreen] scale];
			x.layer.edgeAntialiasingMask = kCALayerLeftEdge | kCALayerRightEdge | kCALayerBottomEdge | kCALayerTopEdge;
		} else {
			x.layer.shouldRasterize = NO;
		}
		x.layer.transform = transform;
	}
}

- (CGFloat)_distanceBetweenCenters {
	NSDictionary *preferences = [NSDictionary dictionaryWithContentsOfFile:SettingsPath];
	BOOL enabled = YES;
	if ([preferences objectForKey:@"enabled"])
		enabled = [[preferences objectForKey:@"enabled"] boolValue];
	if (!enabled)
		return %orig;

	BOOL closerItems = NO;
	if ([preferences objectForKey:@"closerItems"])
		closerItems = [[preferences objectForKey:@"closerItems"] boolValue];
	if (!closerItems)
		return %orig;

	CGFloat distance = %orig;
	return distance/1.3f;
}

// Always make sure you clean up after yourself; Not doing so could have grave consequences!
%end

%hook SBAppSwitcherIconController 

- (void)scrollViewDidScroll:(id)anyScrollView {
	%orig;

	NSDictionary *preferences = [NSDictionary dictionaryWithContentsOfFile:SettingsPath];
	BOOL enabled = YES;
	if ([preferences objectForKey:@"enabled"])
		enabled = [[preferences objectForKey:@"enabled"] boolValue];
	if (!enabled)
		return;

	BOOL fadeIcons = YES;
	if ([preferences objectForKey:@"fadeIcons"])
		fadeIcons = [[preferences objectForKey:@"fadeIcons"] boolValue];

	BOOL rollIcons = NO;
	if ([preferences objectForKey:@"rollIcons"])
		rollIcons = [[preferences objectForKey:@"rollIcons"] boolValue];

	BOOL scaleIcons = YES;
	if ([preferences objectForKey:@"scaleIcons"])
		scaleIcons = [[preferences objectForKey:@"scaleIcons"] boolValue];

	UIScrollView *scrollView = [self valueForKey:@"_scrollView"];

	CGFloat center = [scrollView contentOffset].x + (scrollView.frame.size.width/2.0f);
	UIView *iconContainer = [self valueForKey:@"_iconContainer"];
	for (UIView *x in iconContainer.subviews){
		CGRect frame = x.frame;
		CGFloat centerOfView = frame.origin.x + (frame.size.width/2.0f);
		CGFloat difference = centerOfView-center;
		CGFloat threshold = scrollView.superview.frame.size.width/1.9f;
		if (difference > threshold)
			difference = threshold;
		if (difference < -threshold)
			difference = -threshold;

		CGFloat distanceBetweenCenters = [self _distanceBetweenCenters];
		if (rollIcons)
			x.transform = CGAffineTransformMakeRotation((difference/distanceBetweenCenters)*2.0f*M_PI);
		else
			x.transform = CGAffineTransformIdentity;

		if (scaleIcons){
			CGFloat scale = 1.0f - fabsf(0.25f*(float)(difference/distanceBetweenCenters));
			x.transform = CGAffineTransformScale(x.transform, scale, scale);
		}

		if (fadeIcons)
			x.alpha = 1.0f - (fabsf((float)difference)/threshold);
		else
			x.alpha = 1.0f;
	}
}

%end

%hook SBAppSwitcherController
-(void)animateDismissalToDisplayLayout:(id)displayLayout withCompletion:(id)completion {
	SBAppSwitcherPageViewController *pageController = [self pageController];
	UIScrollView *scrollView = [pageController valueForKey:@"_scrollView"];

	//CGFloat center = [scrollView contentOffset].x + (scrollView.frame.size.width/2.0f);

	for (UIView *x in scrollView.subviews){
		/*CGFloat difference = centerOfView-center;

		CGFloat threshold = scrollView.superview.frame.size.width/1.9f;
		if (difference > threshold)
			difference = threshold;
		if (difference < -threshold)
			difference = -threshold;*/


		[UIView animateWithDuration:0.25 animations:^{
			x.layer.transform = CATransform3DIdentity;
		}];
	}
	%orig;
}

-(void)animatePresentationFromDisplayLayout:(id)displayLayout withViews:(id)views withCompletion:(id)completion {
	%orig;
	SBAppSwitcherPageViewController *pageController = [self pageController];
	UIScrollView *scrollView = [pageController valueForKey:@"_scrollView"];
	[pageController scrollViewDidScroll:scrollView];

	SBAppSwitcherIconController *iconController = [self iconController];
	scrollView = [iconController valueForKey:@"_scrollView"];
	[iconController scrollViewDidScroll:scrollView];
}

-(void)switcherScroller:(id)scroller displayItemWantsToBeRemoved:(id)beRemoved {
	%orig;
	SBAppSwitcherPageViewController *pageController = [self pageController];
	UIScrollView *scrollView = [pageController valueForKey:@"_scrollView"];
	[pageController scrollViewDidScroll:scrollView];

	SBAppSwitcherIconController *iconController = [self iconController];
	scrollView = [iconController valueForKey:@"_scrollView"];
	[iconController scrollViewDidScroll:scrollView];
}
%end